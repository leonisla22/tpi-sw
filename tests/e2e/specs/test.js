// https://docs.cypress.io/api/introduction/api.html

describe("My First Test", () => {
  it("Visits the app root url", () => {
    cy.visit("/");
    cy.contains("h1", "Hello world!");
  });
  it("integration GreetWorld + HelloWorld", () => {
    cy.visit("/");
    cy.get("input")
      .clear()
      .type("My title changes");
    cy.contains("h1", "My title changes");
  });
});
