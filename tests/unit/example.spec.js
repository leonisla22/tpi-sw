import { expect } from "chai";
import { shallowMount } from "@vue/test-utils";
import HelloWorld from "@/components/HelloWorld.vue";
import GreetWorld from "@/components/GreetWorld.vue";

describe("HelloWorld.vue", () => {
  it("renders props.msg when passed", () => {
    const msg = "new message";
    const wrapper = shallowMount(HelloWorld, {
      propsData: { msg }
    });
    expect(wrapper.text()).to.include(msg);
  });
});

describe("GreetWorld.vue", () => {
  it("has input field", () => {
    const wrapper = shallowMount(GreetWorld);
    const input = wrapper.find("input");
    expect(input).to.exist;
  });
  it("sets prop value to input value", () => {
    const value = "new message";
    const wrapper = shallowMount(GreetWorld, {
      propsData: { value }
    });
    const input = wrapper.find("input");
    expect(input.element.value).to.equal(value);
  });
  it("emmits event on input trigger", () => {
    const value = "new message";
    const wrapper = shallowMount(GreetWorld, {
      propsData: { value }
    });
    const input = wrapper.find("input");
    input.trigger("input");
    expect(wrapper.emitted().input[0][0]).to.equal(value);
  });
});
