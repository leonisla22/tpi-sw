# tpi-sw

[![pipeline status](https://gitlab.com/leonisla22/tpi-sw/badges/master/pipeline.svg)](https://gitlab.com/leonisla22/tpi-sw/-/commits/master) [![coverage report](https://gitlab.com/leonisla22/tpi-sw/badges/master/coverage.svg)](https://gitlab.com/leonisla22/tpi-sw/-/commits/master)

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Run your unit tests
```
yarn test:unit
```

### Run your end-to-end tests
```
yarn test:e2e
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
